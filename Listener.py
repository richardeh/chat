"""
Listener.py
Module for listening for incoming chat connections

Author: Richard Harrington
Date Created: 10/24/2013
last Updated: 10/28/2013
"""

import socket,sys,threading,socketserver

HOST=socket.gethostbyname(socket.gethostname())
PORT=12345

class handler(socketserver.BaseRequestHandler):

    def handle(self):
        data = str(self.request.recv(1024),'ascii')
        cur_thread=threading.current_thread()
        response = bytes("{}: {}".format(cur_thread.name,data),'ascii')
        self.request.sendall(response)
            
class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

if __name__ == "__main__":
        server=ThreadedTCPServer((HOST,PORT),handler)
        ip,port=server.server_address
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        print("Server loop running in thread:", server_thread.name)

